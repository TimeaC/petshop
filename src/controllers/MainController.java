package controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import model.Animal;
import model.Programare;
import util.DatabaseUtil;

public class MainController implements Initializable {

	@FXML
	private ListView<Programare> appointmentsList;

	@FXML
	private Label animalName;
	
	@FXML
	private Label animalGender;
	
	@FXML
	private Label animalAge;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUp();
		
		List<Programare> appointments = dbUtil.getAppointments();
		appointmentsList.setItems(FXCollections.observableArrayList(appointments));		
		
		// set the selectedItemChanged listener
		appointmentsList.getSelectionModel().selectedItemProperty().addListener(
				(programare, arg11, arg2) -> updateAnimalData(programare.getValue().getAnimal())	
		);
		appointmentsList.getSelectionModel().select(0);
		
		dbUtil.closeEntityManager();
	}

	private void updateAnimalData(Animal animal) {
		animalName.setText(animal.getName());		
		animalAge.setText(String.valueOf(animal.getAge()));
	
		String gender = String.valueOf(animal.getSex()).equals("M") ? "Male" : "Female";
		animalGender.setText(gender);		
	}

}
