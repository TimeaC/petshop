package tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import util.DateFormatter;

public class DateFormatterTest {

	@Test 
	public void testFormat() {
		String format = "dd/MM/yyyy";
		Date date = Calendar.getInstance().getTime();
		
		DateFormat formatter = new SimpleDateFormat(format);		
		String date1 = formatter.format(date);
		
		DateFormatter.getInstance().setFormat(format);
		
		String date2 = DateFormatter.getInstance().format(date);
		
		Assert.assertEquals(date1, date2);	
	}
}
