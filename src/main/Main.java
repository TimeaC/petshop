package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Animal;
import util.DatabaseUtil;

public class Main extends Application {

	public static void main(String[] args) throws Exception {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("../controllers/MainView.fxml"));
		
		Scene scene = new Scene(root, 900, 600);
		
		primaryStage.setTitle("PetShop");
		primaryStage.setScene(scene);
		primaryStage.show();		
	}

}

/*
 * 
 */
