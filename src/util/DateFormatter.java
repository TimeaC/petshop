package util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

	private static String format = "dd/MM/yyyy";
	private static DateFormatter instance;
	
	private DateFormatter() {	}
	
	public static DateFormatter getInstance() {
		if (instance == null) {
			instance = new DateFormatter();
		}
		return instance;
	}
	
	/**
	 * Returns the format of the formatter
	 * @return the format as String
	 */
	public static String getFormat() {
		return format;
	}
	
	/** 
	 * Sets the format of the formatter.
	 * @param dateFormat the format as String
	 */
	public static void setFormat(String dateFormat) {
		format = dateFormat;
	}
	
	public String format(Date date) {
		DateFormat formatter = new SimpleDateFormat(DateFormatter.format);
		return formatter.format(date);
	}
}
