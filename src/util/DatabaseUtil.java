package util;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Personalmedical;
import model.Programare;

public class DatabaseUtil {
	
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public void setUp() {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShopJPA");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	/**
	 * Save the animal in the database.
	 * @param animal the animal to be saved
	 */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	
	/**
	 * Updates the animal in the database.
	 * @param id
	 * @param newName
	 * @param newSex
	 * @param newAge
	 */
	public void updateAnimal(int id, String newName, char newSex, int newAge) {
		Animal animal = entityManager.find(Animal.class, id);
		entityManager.getTransaction().begin();
		animal.setName(newName);
		animal.setSex(newSex);
		animal.setAge(newAge);
		entityManager.getTransaction().commit();
	}
	
	public void savePersonalMedical(Personalmedical personalMedical) {
		entityManager.persist(personalMedical);
	}
	//update
	public void updatePersonalmedical(int id, String newFirstName, String newLastName, int newAge) {
		Personalmedical personalMedical = entityManager.find(Personalmedical.class, id);
		entityManager.getTransaction().begin();
		personalMedical.setFirstName(newFirstName);
		personalMedical.setLastName(newLastName);
		personalMedical.setAge(newAge);
		entityManager.getTransaction().commit();
	}
	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}
	//update
	public void updateProgramare(int id, Animal idAnimal,Personalmedical idPersonalMedical, Date newDate) {
		Programare programare = entityManager.find(Programare.class, id);
		entityManager.getTransaction().begin();
		programare.setAnimal(idAnimal);
		programare.setPersonalmedical(idPersonalMedical);
		programare.setDate(newDate);
		entityManager.getTransaction().commit();
	}
	
	public List<Programare> getAppointments() {
		return entityManager.createNamedQuery("Programare.findAll", Programare.class).getResultList();
	}
	
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	//functie care opreste entitatea
	public void closeEntityManager() {
		entityManager.close();
	}
	
	//functie in care vom printa rezultatele din baza de date
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from PetShop.Animal", Animal.class)
				.getResultList();
		for(Animal animal : results) {
			System.out.println("Animal :" + animal.getName() + " has ID: " + animal.getIdAnimal());
		}
	}
	
}
